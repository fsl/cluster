include ${FSLCONFDIR}/default.mk

PROJNAME = cluster

LIBS = -lfsl-warpfns -lfsl-meshclass -lfsl-basisfield -lfsl-newimage -lfsl-miscmaths -lfsl-NewNifti  -lfsl-cprob -lfsl-utils -lfsl-znz

XFILES = smoothest fsl-cluster connectedcomp

SCRIPTS = cluster2html

all: ${XFILES}

smoothest: smoothest.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

infertest: infertest.o infer.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

# We name the binary "fsl-cluster"
# to avoid collision with the
# graphviz "cluster" command.
fsl-cluster: cluster.o infer.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}

connectedcomp: connectedcomp.o
	${CXX} ${CXXFLAGS} -o $@ $^ ${LDFLAGS}
